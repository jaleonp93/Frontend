import React, { useState, createContext } from "react";
import axios from "axios";

const InstallationContext = createContext();

function InstallationProvider({ children }) {
  const [installations, setIntstallations] = useState([]);

  const [installActive, setInstallActive] = useState({
    id: 1000,
    name: "Ninguna Instalación",
    location: [41.3, 2.0167],
  });

  const getInstallation = (install) => {
    /*  installations.find((item) => item.name === install); */

    setInstallActive(installations.find((item) => item.name === install));
  };

  const getAllInstallations = async () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    };

    const url = "http://192.168.1.198:8085/getInstallations";

    try {
      const { data } = await axios.get(url, config);
      setIntstallations(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <InstallationContext.Provider
        value={{
          installations,

          getInstallation,
          installActive,
          setIntstallations,
          getAllInstallations,
          setInstallActive,
        }}
      >
        {children}
      </InstallationContext.Provider>
    </div>
  );
}
export { InstallationContext, InstallationProvider };