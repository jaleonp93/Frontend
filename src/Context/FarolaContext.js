import React, { useState, createContext } from "react";
import axios from 'axios'

const FarolaContext = createContext();

function FarolaProvider({ children }) {
  const [listFarol, setListFarol] = useState([]);
  const [listFilteredFarol, setListFilteredFarol] = useState([]);
  const [farolActive, setFarolActive] = useState([]);
  const [farolaSelect, setFarolaSelect]=useState(false);
  const [farolaSuccess, setFarolaSuccess] = useState(false);

  const [farolaMapa, setFarolaMapa] = useState("");

  const getAllLamps = async (inst) => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    };

    const url = `http://192.168.1.198:8085/getStreetLights?id_inst=${inst.id}`;

    try {
      
      const { data } = await axios.get(url,config);
      
        setListFarol(data.data);
      
    } catch (error) {
      console.log(error);
    }
  };

  const getLamp = async (farola_id) => {
    setFarolActive(listFarol.find((farola) => farola.id === farola_id))
  };

  const farolFilteredMethod = (install) => {
    const farolFilter = listFarol.filter(
      (item) => install.name === item.Installation
    );
    setListFilteredFarol(farolFilter);
  };
  
  return (
    <FarolaContext.Provider
      value={{ 
        listFarol, 
        listFilteredFarol, 
        farolFilteredMethod, 
        getAllLamps, 
        getLamp, 
        farolActive, 
        setFarolaSelect, 
        farolaSelect, 
        setFarolaSuccess, 
        farolaSuccess,
        farolaMapa,
        setFarolaMapa
      }}
    >
      {children}
    </FarolaContext.Provider>
  );
}
export { FarolaProvider, FarolaContext };