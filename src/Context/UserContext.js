import React, { useState, createContext } from "react";
import axios from "axios";

const UserContext = createContext();

function getCartFromLocalStorage() {
  return localStorage.getItem("auth")
    ? JSON.parse(localStorage.getItem("auth"))
    : false;
}

function UserProvider({ children }) {
  const [auth, setAuth] = useState(getCartFromLocalStorage());

  const [users, setUsers] = useState([]);
  const [activoModals, setActivoModals] = useState(false);
  const [modalValidation, setModalValidation] = useState(false);

  const [userInfo, setUserInfo] = useState({
    id: "",
    email: "",
    password: "",
  });

  /* FUNCION PARA GET: USUARIOS TECNICO */

  const getAllTech = async () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    };

    const url = "http://192.168.1.198:8085/getTechnicians";

    try {
      const { data } = await axios.get(url, config);
      setUsers(data.data);
    } catch (error) {
      console.log(error);
    }
  };

  /* FUNCION PARA OBTENER RELACION USUARIO - INSTALACION */
  const newArray = (userTech, installationNew) => {
    let newArreglo = userTech.map(function (user) {
      let newUser = JSON.parse(JSON.stringify(user));
      let result = installationNew.filter((x) => {
        return x.id === newUser.id_inst;
      });
      newUser.id_Installation = result.length > 0 ? result[0].name : "Ninguna";
      return newUser;
    });
    return newArreglo;
  };

  /*   FUNCION PARA EL PUT AL BACKEND DEL USUARIO RESPECTO A LA INSTALACION */

  const updateUserInst = async(userArray, idInst) => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    };

    const url = "http://192.168.1.198:8085/setTechniciansToInst";
    const idUsers = userArray.map((x) => x.id);
    const objectPost = {
      'id_inst': idInst,
      'id_tech': idUsers
    }
    try {
      await axios.post(url,objectPost,config);
      setModalValidation(true);
    }
    catch(error) {
      console.log(error);
    }
  };

  return (
    <UserContext.Provider
      value={{
        users,
        auth,
        getAllTech,
        setAuth,
        userInfo,
        newArray,
        updateUserInst,
        setActivoModals,
        activoModals,
        modalValidation,
        setModalValidation,
        setUserInfo,
      }}
    >
      {children}
    </UserContext.Provider>
  );
}

export { UserProvider, UserContext };