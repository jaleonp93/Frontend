import React, { useContext, useEffect } from "react";
import { Form } from "react-bootstrap";
import { InstallationContext } from "../Context/InstallationContext";
import { UserContext } from "../Context/UserContext";

export default function MapDropdownButton() {

  const isntallActAux = {
    id: 1000,
    name: "Ninguna Instalación",
    location: [41.3, 2.0167],
  };

  const {
    getAllInstallations,
    installations,
    getInstallation,
    setInstallActive,
  } = useContext(InstallationContext);

  const { auth } = useContext(UserContext);

  useEffect(() => {
    if (auth) {
      getAllInstallations();
      
    }
  }, [auth]);

  const handleSelect = (e) => {
    setInstallActive(isntallActAux);
    if (e.target.value === "Ninguna Instalación") {
    } else {
      getInstallation(e.target.value);
    }
  };

  return (
    <>
      <div className="show-installs">
        <Form.Control
          as="select"
          className="pueblos-select my-1 mr-sm-2"
          id="inlineFormCustomSelectPref"
          custom
          onChange={handleSelect}
        >
          <option value={isntallActAux.name}>{isntallActAux.name}</option>
          {installations.map((inst) => {
            return (
              <option key={inst.id} value={inst.name}>
                {inst.name}
              </option>
            );
          })}
        </Form.Control>
      </div>
    </>
  );
}