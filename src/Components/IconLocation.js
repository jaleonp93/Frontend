import L from "leaflet";

import mapaIcon from "../assets/farola-blue.svg";

export default function IconLocation() {
  var miIcono = L.icon({
    iconUrl: mapaIcon,
    iconSize: [30, 30],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  });

  return miIcono;
}