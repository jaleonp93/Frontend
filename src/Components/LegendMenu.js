import React from 'react'

export default function LegendMenu() {
    return (
        <div className='legend-menu'>
            <h4>Leyenda del Mapa</h4>
            <ul>
                <li>Cuando es <span className='success-lamp'>A</span> el estado de la farola es excelente</li>
                <li>Cuando es <span className='danger-lamp'>B</span> el estado de la farola es malo</li>
                <li>Cuando es <span className='warning-lamp'>C</span> el estado de la farola es bueno</li>
                <li>Cuando es <span className='no-visited-lamp'>D</span> la farola no ha sido revisada</li>
                <li>Cuando es <span className='visited-lamp'>color violeta</span> la farola ya tiene realizado un mantenimiento</li>
            </ul>
        </div>
    )
}
