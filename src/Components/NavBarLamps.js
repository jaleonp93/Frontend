import React, { useState } from "react";
import { Button } from "react-bootstrap";

import BarShowTop from "./BarShowTop";

export default function NavBarLamps() {
  const [show, setshow] = useState(false);

  return (
    <div className="collapse-bar">
      {show ? <BarShowTop /> : null}
      <Button className="ml-1" onClick={() => setshow(!show)}>
        Expandir
      </Button>
    </div>
  );
}
