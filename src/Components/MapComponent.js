import React, { useState, useContext, useEffect, Fragment } from "react";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  Tooltip
} from "react-leaflet";
import { FarolaContext } from "../Context/FarolaContext";
import { InstallationContext } from "../Context/InstallationContext";
import { UserContext } from "../Context/UserContext";
import { ListGroup, Button } from "react-bootstrap";
import L from "leaflet";
import MapDropdownButton from "./MapDropdownButton";
/* import ModalInforme from "./ModalInforme"; */
import LocationMarker from "./LocationMarker";
import NavBarLamps from "../Components/NavBarLamps";
import "../App.css";
import LegendMenu from './LegendMenu';

/* ICONOS */
import f_blue from "../assets/mark-blue.svg";
import f_red from "../assets/mark-red.svg";
// import f_yellow from "../assets/mark-yellow.svg";
import f_green from "../assets/mark-green.svg";
import f_violet from "../assets/mark-violet.svg";
import f_orange from '../assets/mark-orange.svg';

export default function MapComponent({ history }) {
  //Contexto Instalacion
  const { installActive, setIntstallations } = useContext(InstallationContext);
  //Contexto Farolas
  const { listFarol, getAllLamps, setFarolaMapa, farolActive, setListFarol, farolaSelect } = useContext(FarolaContext);
  //Contexto Usuarios
  const { auth, setUsers } = useContext(UserContext);

  const [center, setCenter] = useState({ lat: "41.501764", lng: "2.245465" });
  useEffect(() => {
    if (auth) {
      const interval = setInterval(() => {
        getAllLamps(installActive);
      }, 3000)
      return () => clearInterval(interval)
    }

    setCenter(installActive.location);
  }, [installActive, auth]);

  var myIconBlue = L.icon({
    iconUrl: f_blue,
    iconSize: [25, 25],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  });

  var myIconRed = L.icon({
    iconUrl: f_red,
    iconSize: [25, 25],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  });
  var myIconOrange = L.icon({
    iconUrl: f_orange,
    iconSize: [25, 25],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  });
  var myIconGreen = L.icon({
    iconUrl: f_green,
    iconSize: [25, 25],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  });
  var myIconViolet = L.icon({
    iconUrl: f_violet,
    iconSize: [25, 25],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  });

  const handleLogout = (e) => {
    e.preventDefault();
    setFarolaMapa("");
    localStorage.setItem("auth", false);
    // setUsers('');
    // setListFarol('');
    // setIntstallations('');
    history.push("/");
  };

  const [state, setstate] = useState({
    currentLocation: {
      lat: "41.501764",
      lng: "2.245465",
    },
    zoom: 15,
  });

  // const markerClick = (station) => {
  //   console.log(station);
  // };

  return (
    <div className="maps">
      <MapContainer zoom={15} center={state.currentLocation} >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {listFarol.map((place) => {

          var pos = place.location;

          var myIcon = place.next_maintenance !== "-"
            ? myIconViolet
            : place.state === "A"
              ? myIconGreen
              : place.state === "B"
                ? myIconRed
                : place.state === "C"
                  ? myIconOrange
                  : myIconBlue;

          return (
            <Fragment key={place.id}>
              <Marker position={pos} icon={myIcon}
                eventHandlers={{
                  click: () => {
                    setFarolaMapa(place.id);
                  },
                }}
              >

                {farolActive.id === place.id && farolaSelect ? (
                  <Tooltip permanent interactive>{place.id}

                  </Tooltip>
                ) : null}
                <Popup>
                  <ListGroup>
                    {place.next_maintenance !== "-" ? (
                      <ListGroup.Item variant="secondary"></ListGroup.Item>
                    ) : place.state === "A" ? (
                      <ListGroup.Item variant="success"></ListGroup.Item>
                    ) : place.state === "B" ? (
                      <ListGroup.Item variant="danger"></ListGroup.Item>
                    ) : place.state === "C" ? (
                      <ListGroup.Item variant="warning"></ListGroup.Item>
                    ) : (
                      <ListGroup.Item variant="primary"></ListGroup.Item>
                    )}
                    <ListGroup.Item>ID: {place.id}</ListGroup.Item>
                    <ListGroup.Item> Familia: {place.family}</ListGroup.Item>
                    <ListGroup.Item>
                      Ubicación: {place.location[0]}, {place.location[1]}
                    </ListGroup.Item>
                    <ListGroup.Item>Tipo luz: {place.light}</ListGroup.Item>
                    <ListGroup.Item>
                      Última visita: {place.last_maintenance}
                    </ListGroup.Item>
                    <ListGroup.Item>
                      Próxima visita: {place.next_maintenance}
                    </ListGroup.Item>
                    {/* <ListGroup.Item
                      variant="secondary"
                      onClick={() => setModalShow(true)}
                    >
                      Informe
                    </ListGroup.Item> */}
                  </ListGroup>
                </Popup>
              </Marker>
            </Fragment>
          );
        })}
        <LocationMarker position={installActive} />
      </MapContainer>
      <Button className="button-logout" onClick={handleLogout}>
        Salir
      </Button>
      <MapDropdownButton />
      <LegendMenu />
      <NavBarLamps />
    </div>
  );
}
