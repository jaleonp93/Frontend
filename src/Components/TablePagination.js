import React, { useContext, useEffect, useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import { FarolaContext } from '../Context/FarolaContext';
import { InstallationContext } from "../Context/InstallationContext";
import { UserContext } from "../Context/UserContext";
import ModalInform from '../Modals/ModalInform';

export default function TablePagination() {

  const { SearchBar } = Search;
  const { installActive } = useContext(InstallationContext);
  const { auth } = useContext(UserContext);
  const {
    listFarol,
    getAllLamps,
    getLamp,
    setFarolaSelect,
    farolaSelect,
    farolaMapa
  } = useContext(FarolaContext);

  const [modalShowInform, setModalShowInform] = useState(false);

  const [informeActivo, setInformeActivo] = useState({});

  const openModal = (row) => {
    setInformeActivo(row);
    setModalShowInform(true);
  };

  let tableHeight = "100px";

  useEffect(() => {
    if (auth) {
      const interval = setInterval(() => {
        getAllLamps(installActive);
      }, 10000)
      return () => clearInterval(interval)
    }
  }, [auth, farolaMapa]);

  const selectRow = {
    mode: "radio",
    clickToSelect: true,
    hideSelectColumn: true,
    onSelect: (isSelect, row) => {
      getLamp(isSelect.id);
      setFarolaSelect(row);
    },
    // classes: () => farolaSelect ? 'clase2' : 'clase1'
    style: () => {
      const backgroundColor = farolaSelect ? "#005BBB" : '#FFF';
      return { backgroundColor };
    }
  };

  // function customMatchFunc(farolaMapa, value) {
  //   return value.startsWith(farolaMapa);
  // }

  function informFormatter(cell, row) {
    return <h5 className='notes'> Informes</h5>;
  }

  function locationFormatter(cell, row) {
    return <p>{cell[0]} - {cell[1]}</p>;
  }
  const columnsFilter = [
    {
      dataField: "id",
      headerAlign: "center",
      text: "ID",
      align: "center",
      sort: true,
      filterValue: (cell, row) => {
      }
    },
  ];

  const columns = [
    {
      dataField: "id",
      headerAlign: "center",
      text: "ID",
      filter: textFilter({
        placeholder: 'Entrar ID'
      }),
      align: "center",
      sort: true,
    },
    {
      dataField: "family",
      headerAlign: "center",
      text: "Familia",
      filter: textFilter({
        placeholder: 'Entrar Familia'
      }),
      align: "center",
      sort: true,
    },
    {
      dataField: "light",
      headerAlign: "center",
      text: "Tipo Luz",
      align: "center",
      sort: true,
      filter: textFilter({
        placeholder: 'Entrar Tipo Luz'
      }),
    },

    {
      dataField: "pdl_code",
      headerAlign: "center",
      text: "Código PDL",
      align: "center",
      sort: true,
      filter: textFilter({
        placeholder: 'Entrar Código PDL'
      }),
      searchable: false,
    },
    {
      dataField: "location",
      headerAlign: "center",
      text: "Localización",
      align: "center",
      sort: true,
      filter: textFilter({
        placeholder: 'Entrar Localización'
      }),
      formatter: locationFormatter,
    },
    {
      dataField: "state",
      headerAlign: "center",
      text: "Estado",
      align: "center",
      sort: true,
      filter: textFilter({
        placeholder: 'Entrar Estado'
      }),
    },
    {
      dataField: "last_maintenance",
      headerAlign: "center",
      text: "Últ. mant.",
      align: "center",
      sort: true,
      // filterValue: (cell, row) => console.log(cell) ,
      // filter: textFilter({
      //   placeholder: 'Entrar Últ. Fecha'
      // }),
    },
    {
      dataField: "next_maintenance",
      headerAlign: "center",
      text: "Próx. mant.",
      align: "center",
      sort: true,
      // filter: textFilter({
      //   placeholder: 'Entrar Póx. Fecha'
      // }),
    },
    {
      dataField: "notes",
      headerAlign: "center",
      text: "Notas",
      align: "center",
      sort: true,
      formatter: informFormatter,
      events: {
        onClick: (e, column, columnIndex, row, rowIndex) => {
          /*   console.log(e); */
          /* console.log(column); */
          /*       console.log(columnIndex);
          
          console.log(rowIndex); */

          openModal(row);
        },
      },

    },
  ];

  return (
    <>
      <div className="table-overflow">
        <ToolkitProvider
          keyField="id"
          data={listFarol}
          columns={columnsFilter}
          selectRow={selectRow}
          search={{ defaultSearch: `${farolaMapa}` }}
        >
          {(props) => (
            <div>
              <h6>Ingrese su contenido de búsqueda:</h6>
              <SearchBar {...props.searchProps} placeholder='Entre su búsqueda' />

              <BootstrapTable
                {...props.baseProps}
                bootstrap4
                scroll
                keyField="id"
                data={listFarol}
                columns={columns}
                filter={filterFactory()}
                headerWrapperClasses="foo"
                filterPosition="top"
                selectRow={selectRow}
                striped
                hover
                bordered
                small
                maxHeight={tableHeight}
                wrapperClasses="table-responsive"
              />
            </div>
          )}
        </ToolkitProvider>
      </div>
      <ModalInform
        show={modalShowInform}
        onHide={() => setModalShowInform(false)}
        informe={informeActivo}
      />
    </>
  );
}