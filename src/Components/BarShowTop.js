import React, { useContext, useState } from "react";

import ModalsTech from "../Modals/ModalsTech";
import ModalsCalendar from "../Modals/ModalsCalendar";

import { Button, Alert } from "react-bootstrap";

import { AiOutlineUserAdd } from "react-icons/ai";
import { GoCalendar } from "react-icons/go";
import TablePagination from "./TablePagination";
import { UserContext } from "../Context/UserContext";
import { FarolaContext } from '../Context/FarolaContext';

export default function BarShowTop() {
  const [modalShowTech, setModalShowTech] = useState(false);
  const [modalShowCalendar, setmodalShowCalendar] = useState(false);
  const { setActivoModals, modalValidation, setModalValidation } = useContext(UserContext);
  const {farolaSelect, farolaSuccess, setFarolaSuccess} = useContext(FarolaContext);

  const openModal = (e) => {
    setActivoModals(true);
    setModalShowTech(true);
  };

  return (
    <div className="nav-bar-heigth">
      <div className="header-lamps">
        <Button
          className="ml-2 mt-2 mb-2"
          variant="primary"
          onClick={openModal}
        >
          <AiOutlineUserAdd />
        </Button>

        <ModalsTech
          show={modalShowTech}
          onHide={() => setModalShowTech(false)}
        />

        {farolaSelect ? <Button
          className="ml-2 mt-2 mb-2"
          variant="primary"
          onClick={() => setmodalShowCalendar(true)}
        >
          <GoCalendar />
        </Button> : null}
        <ModalsCalendar
          show={modalShowCalendar}
          onHide={() => setmodalShowCalendar(false)}
        />
        {farolaSuccess ? <Alert className='ml-2 mt-2 mb-2 user-inserted' variant='success' onClose={() => setFarolaSuccess(false)} dismissible>
          Mantenimiento programado correctamente</Alert> : null}
        {modalValidation ? <Alert className='ml-2 mt-2 mb-2 user-inserted' variant='success' onClose={() => setModalValidation(false)} dismissible>
          Técnicos agregados correctamente</Alert> : null}
      </div>
      <div className="bodylamps">
        <TablePagination />
      </div>
    </div>
  );
}