import React, {useState} from 'react';
import Pagination from 'react-bootstrap'



function PaginationLamps() {

    const [paginationLamps, setPaginationLamps] = useState([
        'lamp1','lamp2','lamp3','lamp4','lamp5','lamp6','lamp7','lamp8','lamp9','lamp10','lamp11','lamp12','lamp13','lamp14','lamp15','lamp16','lamp17','lamp18','lamp19'
    ]);
    const [currentPage, setcurrentPage] = useState(1);
    const [itemsPerPage, setitemsPerPage] = useState(5);

    const pages= [];
    for (let i = 1; i <= Math.ceil(paginationLamps.length/itemsPerPage); i++){
        pages.push(i);
    }
    
    return (
        <div>
            <Pagination>
                <Pagination.First />
                <Pagination.Prev />
                <Pagination.Item>{pages[0]}</Pagination.Item>
                <Pagination.Ellipsis />

                <Pagination.Item>{10}</Pagination.Item>
                <Pagination.Item>{11}</Pagination.Item>
                <Pagination.Item active>{12}</Pagination.Item>
                <Pagination.Item>{13}</Pagination.Item>
                <Pagination.Item disabled>{14}</Pagination.Item>

                <Pagination.Ellipsis />
                <Pagination.Item>{20}</Pagination.Item>
                <Pagination.Next />
                <Pagination.Last />
            </Pagination>
        </div>
    );
}

export default PaginationLamps;