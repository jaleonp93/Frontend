import { Marker, Popup, useMapEvents } from "react-leaflet";

import pueblo from "../assets/pueblo.svg";

import React from "react";
import L from "leaflet";

export default function LocationMarker({ position }) {
  var myIcon = L.icon({
    iconUrl: pueblo,
    iconSize: [25, 25],
    iconAnchor: [12.5, 41],
    popupAnchor: [0, -41],
  });

  const map = useMapEvents({
    
    dblclick() {
      map.locate();
    },
    locationfound(e) {
      map.flyTo(position.location, map.getZoom());
    },
  });
  return (
    <Marker position={position.location} icon={myIcon}>
      <Popup>{position.name}</Popup>
    </Marker>
  );
}