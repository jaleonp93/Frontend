import "./App.css";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LoginPage from "./Views/LoginPage";
import MapComponent from "./Components/MapComponent";
import ProtectedRoutes from "./Components/ProtectedRoutes";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={LoginPage} />
        <ProtectedRoutes exact path="/map" component={MapComponent} />
      </Switch>
    </Router>
  );
}

export default App;
