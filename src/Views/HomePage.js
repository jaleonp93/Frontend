import React from "react";

import MapDropdownButton from "../Components/MapDropdownButton";
import NavBarLamps from '../Components/NavBarLamps'

export default function HomePage() {
  return (
    <>
    {/* Boton add user */}

      <MapDropdownButton />
      
      <NavBarLamps />
    </>
  );
}
