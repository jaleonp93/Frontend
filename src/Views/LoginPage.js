import React, { useContext, useState } from "react";

import { UserContext } from "../Context/UserContext";
import { Form, Button, Image, InputGroup, Alert } from "react-bootstrap";
import Citelum from "../assets/Nueva carpeta/CITELUM_LOGO_RGB_600.jpg";
import axios from "axios";
import { AiOutlineUser } from 'react-icons/ai';
import { GiPadlock } from 'react-icons/gi'

export default function LoginPage({ history }) {
  const {  setAuth } = useContext(UserContext);

  const [username, setName] = useState("");
  const [password, setPassword] = useState("");
  const [login, setLogin] = useState(false);
  const apiKey = "QhkyEfaCgbRbqDgfbBaVVFt2Jwt6Lzyk";
  const url = "http://192.168.1.198:8085/auth";

  const handleSubmit = (event) => {
    event.preventDefault();

    const data = {
      username: username,
      password: password,
      apiKey: apiKey,
    };
    axios
      .post(url, data)
      .then((res) => {
        localStorage.setItem("access_token", res.data.access_token);
        localStorage.setItem("auth", true);
        //const bool2 = localStorage.getItem("auth", true);
        setAuth(localStorage.getItem("auth"));
        history.push("/map");

      })
      .catch((e) => {
        console.log(e);
        setLogin(true);
      });
  };
  function validateForm() {
    return username.length > 0 && password.length > 0;
  }

  return (
    <div className="login">
      <Image className='image-front' src={Citelum} alt="Imagen de usuario" fluid />

      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="formGroupName">
          <Form.Label>Nombre</Form.Label>
          <InputGroup className="mb-2">
            <InputGroup.Prepend>
            <InputGroup.Text><AiOutlineUser /></InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control
            placeholder='Entrar nombre '
            autoFocus
            type="text"
            value={username}
            onChange={(e) => setName(e.target.value)}
          />
          </InputGroup>
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <Form.Label>Contraseña</Form.Label>
          <InputGroup className="mb-2">
          <InputGroup.Prepend>
          <InputGroup.Text><GiPadlock /></InputGroup.Text>
        </InputGroup.Prepend>
          <Form.Control
            placeholder='Entrar contraseña'
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          </InputGroup>
        </Form.Group>

        <Button block size="lg" type="submit" disabled={!validateForm()}>
          Entrar
        </Button>
        {
          login ?  <Alert className='mt-3 alert-changes' variant='danger' onClose={() => setLogin(false)} dismissible>Nombre o contraseña incorrectos</Alert> : null
        }        
      </Form>
    </div>
  );
}