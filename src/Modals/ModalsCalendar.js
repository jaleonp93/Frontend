import React, { useState, useContext } from "react";
import { Modal, Button, Alert } from "react-bootstrap";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { InstallationContext } from "../Context/InstallationContext";
import axios from "axios";
import { FarolaContext } from "../Context/FarolaContext";

export default function ModalsCalendar(props) {
  const [daysCalendar, setDaysCalendar] = useState("");
  const [alertCalendar, setAlertCalendar] = useState(false);
  const { installActive } = useContext(InstallationContext);
  const { farolActive, setFarolaSuccess } = useContext(FarolaContext);

  let saveCalendar = {};

  const onChangeDays = (value, event) => {
    setDaysCalendar(value.toLocaleDateString());
  };

  const sendCalendar = async () => {
    const strCalendar = daysCalendar.split("/");
    const day = parseInt(strCalendar[0]);
    const month = parseInt(strCalendar[1]);
    const year = parseInt(strCalendar[2]);
    saveCalendar = {
      id_inst: installActive.id,
      id_street_light: farolActive.id,
      date: {
        day: day,
        month: month,
        year: year,
      },
    };
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    };
    const url = "http://192.168.1.198:8085/addMaintenance";
    try {
      await axios.post(url, saveCalendar, config);
      setFarolaSuccess(true);
      props.onHide();
    } catch (e) {
      console.log(e);
      setAlertCalendar(true);
    }
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Asignar mantenimiento a luminarias
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Calendar
          onChange={onChangeDays}
          calendarType="US"
          defaultView="month"
          selectRange={false}
        />
      </Modal.Body>
      <Modal.Footer>
        {alertCalendar ? (
          <Alert
            className="mt-2 alert-changes"
            variant="danger"
            onClose={() => setAlertCalendar(false)}
            dismissible
          >
            Esta farola ya posee un mantenimiento
          </Alert>
        ) : null}
        <Button onClick={sendCalendar}>Aceptar </Button>
        <Button onClick={props.onHide}>Cancelar</Button>
      </Modal.Footer>
    </Modal>
  );
}
