import React from "react";

import { Modal, Button } from "react-bootstrap";

export default function ModalInform(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <h1>{props.informe.family}</h1>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Notas</h4>
        <p>{props.informe.notes}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}