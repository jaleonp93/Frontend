import React, { useState, useContext, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory from "react-bootstrap-table2-filter";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.css";

import { UserContext } from "../Context/UserContext";
import { InstallationContext } from "../Context/InstallationContext";

export default function ModalsTech(props) {
  const {
    users,
    updateUserInst,
    auth,
    getAllTech,
    activoModals,
    setActivoModals,
  } = useContext(UserContext);

  const { installActive } = useContext(InstallationContext);

  const [tech, setTech] = useState([]);

  useEffect(() => {
    if (auth && activoModals) {
      getAllTech();
      /*  setUsers_Instal(newArray(users, installations)); */
    }
    /* 
      Con esto limpiamos el arreglo de tech */

    tech.splice(0, tech.length);

    /*  setActiveTech(userInstallation(users, installActive)); */

    /*    setUsers_Instal(newArray(users, installations)); */
  }, [activoModals]);

  const columns = [
    {
      dataField: "email",
      headerAlign: "center",
      text: "Nombre Técnico",
      sort: true,
      align: "center",
    },
    {
      dataField: "id_inst",
      headerAlign: "center",
      text: "Instalación",
      align: "center",
      sort: true,
    },
  ];

  let tableHeight = "100px";

  const selectRow = {
    clickToSelect: true,
    hideSelectColumn: true,
    mode: "checkbox",
    style: { background: "#005BBB", color: "#fff" },

    onSelect: (isSelect, row) => {
      if (row) {
      
        tech.push(isSelect);
      } else {
        let index = tech.map((x) => x.id).indexOf(isSelect.id);
        tech.splice(index, 1);
      }
    },
    /* onSelectAll: (isSelect, rows) => {
      console.log("Evento OnSelectAll");
      console.log(isSelect);
      isSelect
        ? rows.map((item) => {
            tech.push(item);
            return item;
          })
        : tech.splice(0, tech.length);
      console.log(tech);
    }, */
  };

  function handleAceptar() {
    // if (installActive.title !== "Ninguna") {
    /*   
   ESTA ES LA FUNCION PARA PASAR AL BACKEND LOS USUARIOS Y LA INSTALACION ACTIVA */

    updateUserInst(tech, installActive.id);
    
    /* tech.splice(0, tech.length); */
    setActivoModals(false);
    props.onHide();
    // } else {
    //   alert("no ha seleccionado ninguna instalacion");
    // }
  }

  function handleCancelar() {
    // if (installActive.title !== "Ninguna") {
    /*   
  ESTA ES LA FUNCION PARA PASAR AL BACKEND LOS USUARIOS Y LA INSTALACION ACTIVA
  
   updateUserInst(tech, installActive.id); */

    /* tech.splice(0, tech.length); */
    setActivoModals(false);
    props.onHide();
    // } else {
    //   alert("no ha seleccionado ninguna instalacion");
    // }
  }

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Asignar técnico a instalación
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <BootstrapTable
          bootstrap4
          scroll
          keyField="id"
          data={users}
          columns={columns}
          filter={filterFactory()}
          headerWrapperClasses="foo"
          filterPosition="top"
          selectRow={selectRow}
          striped
          hover
          bordered
          small
          maxHeight={tableHeight}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleAceptar}>Aceptar</Button>
        <Button onClick={handleCancelar}>Cancelar</Button>
      </Modal.Footer>
    </Modal>
  );
}