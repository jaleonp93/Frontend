import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "./bootstrap.min.css";
import "leaflet/dist/leaflet.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

import { UserProvider } from "./Context/UserContext";
import { FarolaProvider } from "./Context/FarolaContext";
import { InstallationProvider } from "./Context/InstallationContext";

ReactDOM.render(
  <UserProvider>
    <InstallationProvider>
      <FarolaProvider>
        <App />
      </FarolaProvider>
    </InstallationProvider>
  </UserProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
